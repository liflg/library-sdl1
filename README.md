Website
=======
http://libsdl.org/

License
=======
zlib license (see the file source/COPYING)

Version
=======
1.2 HEAD

Source
======
SDL-453dce726f24.tar.gz (sha256: 08f243a8cb838ed0b1bd05712a345d22dfcf22558b7ddbdd7c6b7bf49f02545d)

Please note: This file was generated on-the-fly using this url:
 - http://hg.libsdl.org/SDL/archive/453dce726f24.tar.gz

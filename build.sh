#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x$SELECTEDOPTION = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x$SELECTEDOPTION = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x$SELECTEDOPTION = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x$SELECTEDOPTION = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo 'Invalid option selected!'
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        CFLAGS="$CFLAGS -ggdb"
    else
        CFLAGS="$CFLAGS"
    fi

    if [ -z "$OPTIMIZATION" ]; then
        CFLAGS="$CFLAGS -O2"
    else
        CFLAGS="$CFLAGS -O$OPTIMIZATION"
    fi

    export CFLAGS

    #we can not do out-of-source builds :(
    ( cd source
      git clean -df .
      git checkout .
      ./autogen.sh
      ./configure \
        --prefix="$PREFIXDIR" \
        --disable-static \
        --enable-sdl-dlopen \
        --enable-pulseaudio \
        --enable-pulseaudio-shared \
        --enable-alsa \
        --enable-alsa-shared \
        --enable-oss \
        --enable-video-x11-xrandr \
        --enable-video-opengl \
        --disable-arts \
        --disable-nas \
        --disable-esd \
        --disable-input-tslib \
        --disable-video-fbcon
      make -j`nproc` install
      rm -rf "$PREFIXDIR"/{bin,lib/*.la,lib/pkgconfig,share})
}

if [ -z "$MULTIARCHNAME" ]; then
    echo '$MULTIARCHNAME is not set!'
    select_multiarchname
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING "$PREFIXDIR"/lib/LICENSE-sdl1.txt

echo "SDL 1.2.15 for $MULTIARCHNAME is ready."
